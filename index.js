const express = require("express")
const stringify = require('csv-stringify/lib/sync')
const utils = require('./utils')

const app = express()

app.get('/', (req, res) => {
    res.send(`use http://localhost:${port}/generate-csv?rows=200 to generate csv. rows will be the number of records to generate`)
})

app.get('/generate-csv', (req, res) => {
    const emailDomain = '@cc-test.com'
    const {rows=100} = req.query
    const records = [];
    for(let i=0;i<rows;i++){
        const padCount = utils.padstr(i.toString(),8)
        const fname = utils.getRandomName()
        const lname = utils.getRandomName()
        records.push({
            "first_name": fname,
            "last_name": lname,
            "email": `${fname.toLowerCase()}_${lname.toLowerCase()}_${padCount}${emailDomain}`
        })
    }
    const data = stringify(records,{
        header: true,
        columns: ['first_name', 'last_name','email']
      })
    res.attachment('users.csv');
    res.send(Buffer.from(data))
})


const port = 3033
app.listen(port,()=>{
    console.log(`listening on port ${port}`);
})