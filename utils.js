const names = require("./names")

function padstr(num, padlen) {
    const pad_char = '0';
    const pad = new Array(1 + padlen).join(pad_char);
    return (pad + num).slice(-pad.length);
}

function getRandomName(){
    const randomIndex = Math.floor(Math.random() * names.length);
    return names[randomIndex];
}

exports.padstr = padstr
exports.getRandomName = getRandomName